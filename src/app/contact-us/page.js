
import ContactForm from "../components/contact-form";
import Telephones from "../assests/images/telephones.png";
import ChurchAddress from "../assests/images/church address.png";
import Email from "../assests/images/email-address.png";
import Image from "next/image";
import TextWithMedia from "../components/text-with-media";
import Headphone from "../assests/images/Headphone.png";
import ImageTwo from "../assests/images/image-two.png";

export default function ContactUs() {
  return (
    <>
      <div>
        <h2 className="text-center">Contact Us</h2>
        <div className="container pt-5">
          <div className="row gap-3">
            <div className="col ministry-border text-center">
              <Image src={Telephones} alt="" className="ministry-img" />
              <h5 className="pt-5">Telephone number</h5>
              <p>07421706769 </p>
            </div>
            <div className="col ministry-border text-center">
              <Image src={ChurchAddress} alt="" className="ministry-img" />
              <h5 className="pt-5">Church address</h5>
              <p>
                North Block, Kenton School, Drayton Road, Newcastle Upon Tyne
                NE3 3RU{" "}
              </p>
            </div>
            <div className="col ministry-border text-center">
              <Image src={Email} alt="" className="ministry-img" />
              <h5 className="pt-5">Email address</h5>
              <p>info@hispresencenewcastle.org.uk. </p>
            </div>
          </div>
        </div>
      </div>

      <ContactForm />
      <TextWithMedia
        media={ImageTwo}
        title="Stay connected always"
        paragraph="We invite you to join us live and be part of our church meetings
                    to experience the uplifting and inspiring messages firsthand."
        bgcolor="#ffffff"
        buttonLink="https://www.google.com/"
        buttonText="Listen Live"
        linkIcon={Headphone}
        customClasses="stay-connected"
        bgDots={true}
      />
    </>
  );
}
