import ManOnRoof from "../assests/images/man-on-roof.png";
import LadyWithHair from "../assests/images/lady-with-hair.png";
import Image from "next/image";
import TextWithMedia from "../components/text-with-media";
import Headphone from "../assests/images/Headphone.png";
import ImageTwo from "../assests/images/image-two.png";

const OurMission = () => {
  return (
    <div className="px-5">
      <div className="custom-container">
        <h1 className="text-center fw-bolder pb-5">
          We live for the glory of God
        </h1>
        <div className="god-glory-img-container d-flex gap-5 mt-5">
          <Image
            src={LadyWithHair}
            alt="happylady"
            className="position-relative first-god-glory-img-container"
          />
          <Image
            src={ManOnRoof}
            alt="happyman"
            className="position-relative second-god-glory-img-container"
          />
        </div>
      </div>
      <div className="mission-container custom-container pb-5">
        <div className="sub-mission-container">
          <h3 className="text-center fw-bold fs-1">Our Mission</h3>
          <div>
            <p>
              Through meaningful worship, authentic fellowship, and
              compassionate outreach, we seek to glorify God, share His love
              with others, and make a positive impact in our local and global
              communities.
            </p>

            <p>
              Our mission through the preaching of the gospel is to be a beacon
              of hope, healing, and transformation, reflecting the diversity and
              unity found in the body of Christ while raising a generation of
              influencers.
            </p>

            <p>
              These are a people who embody integrity, compassion, and
              innovation, impacting society for the glory of God and the
              betterment of humanity.
            </p>

            <p>
              Together, we envision a future where faith-driven Leaders lead by
              example, transforming communities and ushering in positive change
              across the seven pillars of society
            </p>
          </div>
        </div>
      </div>
      <div className="rccg-container">
        <div className="pt-5">
          <h2 className="fs-1 fw-bold container">RCCG Mission</h2>

          <div className="container pb-5">
            <div className="row gap-3 pt-5 align-items-start">
              <div className="col defined-border rounded-3 d-flex justify-content-center align-items-center flex-column">
                <p className="fw-bolder defined-num">1</p>
                <p className="defined-p defined-num">To make heaven </p>
              </div>
              <div className="col defined-border rounded-3 d-flex justify-content-center align-items-center flex-column">
                <p className="fw-bolder defined-num">2</p>
                <p className="defined-p">
                  To take as many people as possible with us{" "}
                </p>
              </div>
              <div className="col defined-border rounded-3 d-flex justify-content-center align-items-center flex-column">
                <p className="fw-bolder defined-num">3</p>
                <p className="defined-p">
                  To have a member of RCCG in every family of all nations{" "}
                </p>
              </div>
            </div>
            <div className="row gap-3 pt-5 align-items-start">
              <div className="col defined-border rounded-3 d-flex justify-content-center align-items-center flex-column">
                <p className="fw-bolder defined-num">4</p>
                <p className="defined-p">
                  To have a member of RCCG in every family of all nations{" "}
                </p>
              </div>
              <div className="col defined-border rounded-3 d-flex justify-content-center align-items-center flex-column">
                <p className="fw-bolder defined-num">5</p>
                <p className="defined-p">
                  To accomplish No. 2 and 3 above, we will plant churches within
                  five minutes walking distance in every city and town of
                  developing countries and within five minutes driving distance
                  in every city and town of developed countries{" "}
                </p>
              </div>
              <div className="col defined-border rounded-3 d-flex justify-content-center align-items-center flex-column">
                <p className="fw-bolder defined-num">6</p>
                <p className="defined-p">
                  We will pursue these objectives until every nation in the
                  World is reached for Jesus Christ our Lord.{" "}
                </p>
              </div>
            </div>
          </div>
          <TextWithMedia
            media={ImageTwo}
            title="Stay connected always"
            paragraph="We invite you to join us live and be part of our church meetings
                    to experience the uplifting and inspiring messages firsthand."
            bgcolor="#ffffff"
            buttonLink="https://www.google.com/"
            buttonText="Listen Live"
            linkIcon={Headphone}
            customClasses="stay-connected"
            bgDots={true}
          />
        </div>
      </div>
    </div>
  );
};

export default OurMission;
