"use client"

import { usePathname } from "next/navigation";
import Button from "./button";
import Image from "next/image";



const Banner = ({
  title,
  subtitle,
  bgImage,
  buttons = [],
  customTitleClass,
  customSubtitleClass,
  bannerheight = "small",
  customClasses = "",
}) => {
  const { pathname } = usePathname();
  return (
    <div>
      <div className="position-relative">
        <Image
          src={bgImage}
          className={`banner-image banner-${bannerheight} ${
            customClasses ? customClasses : ""
          }`}
          alt="Banner"
        />
        <div className="position-absolute banner-overlaying">
          <div className="overlay">
            <h1
              className={`text-white banner-title ${
                customTitleClass ? customTitleClass : ""
              }`}
            >
              {title}
            </h1>
            <p
              className={`text-white ${
                customSubtitleClass ? customSubtitleClass : ""
              }`}
            >
              {subtitle}
            </p>

            <div className="d-flex align-items-stretch gap-large">
              {buttons.map(
                (
                  { text, bgColor, border, icon, textColor, borderRadius, customName },
                  index
                ) => (
                  <Button
                    key={`buttn-${index}`}
                    text={text}
                    bgColor={bgColor}
                    border={border}
                    icon={icon}
                    textColor={textColor}
                    borderRadius={borderRadius}
                    customName={customName}
                  />
                )
              )}
            </div>
          </div>
        </div>
        <div
          className={`position-absolute banner-overlay ${
            pathname === "/live-stream" ? "livestream-overlay" : ""
          }`}
        />
      </div>
    </div>
  );
};

export default Banner;
