import ExpectationImage from "../assests/images/rectangle.png";
import Icon from "../assests/images/Tick Circle.png";
import Image from "next/image";
import MinistriesImage from "../assests/images/Frame 280.png";

const Expectation = () => {
  const styles = {
    backgroundImage: `url(${ExpectationImage})`,
    backgroundSize: "cover",
    backgroundPosition: "center",
  };
  return (
    <div>
      <div className="position-relative" style={styles}>
        <div className="expectation-overlaying">
          <div className="expectation-presence d-flex flex-column flex-lg-row gap-4 align-items-lg-stretch">
            <div className="overlay-expectation ">
              <h2 className="mb-5 mt-5">What to expect from His presence?</h2>
              <div>
                <div className="expectation-container d-flex mb-4">
                  <div className="icon-sizing">
                    <Image src={Icon} alt="" />
                  </div>

                  <div className="sub-expectation-container">
                    <h5 className="text-start ps-3 fw-bold">
                      What to expect from His presence?
                    </h5>
                    <p className="text-start">
                      Sunday Service starts at 10:00am with soul lifting praise
                      and worship songs.
                    </p>
                  </div>
                </div>
                <div className="expectation-container d-flex mb-4">
                  <div className="icon-sizing">
                    <Image src={Icon} alt="" />
                  </div>
                  <div className="sub-expectation-container">
                    <h5 className="text-start ps-3 fw-bold">
                      Life-transforming encounter through God’s word and prayers
                    </h5>
                    <p className="text-start">
                      Sunday Service starts at 10:00am with soul lifting praise
                      and worship songs.
                    </p>
                  </div>
                </div>
                <div className="expectation-container d-flex mb-4">
                  <div className="icon-sizing">
                    <Image src={Icon} alt="" />
                  </div>
                  <div className="sub-expectation-container">
                    <h5 className="ps-3 fw-bold text-start">
                      A genuine fellowship with one and other
                    </h5>
                    <p className="text-start">
                      Sunday Service starts at 10:00am with soul lifting praise
                      and worship songs.
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="flex-fill">
              <Image
                className="h-100 object-fit-cover"
                src={MinistriesImage}
                alt=""
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Expectation;
