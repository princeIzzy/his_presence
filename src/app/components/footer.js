"use client"

import Buttons from "./button";
import FooterImage from "../assests/images/footerImage.png";
import Youtube from "../assests/images/youtube.png";
import Instagram from "../assests/images/Group (1).png";
import Logo from "../assests/images/logo.png";
import { usePathname } from "next/navigation";
import Image from "next/image";


const Footer = () => {
  const { pathname } = usePathname();
  return (
    <footer>
      <div>
        {pathname === "/live-stream" ? null : (
          <section className={`section-footer-one `}>
            <div>
              <div className="position-relative">
                <Image
                  src={FooterImage}
                  className="section-footer-img"
                  alt="Footer"
                />
                <div className="position-absolute footer-overlaying">
                  <div className="footer-overlay text-white">
                    <h1 className="section-footer-title">
                      Give to support our ministry
                    </h1>
                    <p className="">
                      Every contribution, no matter the size, helps in advancing
                      our faith community.
                    </p>
                    <div className="d-flex align-items-stretch gap-large">
                      <Buttons
                        bgColor="transparent"
                        text="Give now"
                        border="1px solid #fff"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        )}

        <section className="section-footer-two text-white">
          <div className="sub-section-footer-two d-flex">
            <div className="d-flex sub-section-footer-container justify-content-md-between">
              <div className="sub-section-one d-flex flex-column">
                <Image src={Logo} alt="Logo" className="footer-logo" />
                <p>
                  We trust that God works through ordinary people, fostering
                  honesty and authenticity, allowing both individuals and
                  Christian faith to thrive.
                </p>
              </div>
              <div className="sub-section-two d-flex flex-column">
                <h5>About</h5>
                <ul className="list-unstyled">
                  <li>About us</li>
                  <li>Our mission</li>
                  <li>Core beliefs</li>
                </ul>
              </div>
              <div className="sub-section-three d-flex flex-column">
                <h5>Links</h5>
                <ul className="list-unstyled">
                  <li>Live stream</li>
                  <li>Contact us</li>
                  <li>Givings</li>
                  <li>Ministries</li>
                </ul>
              </div>
              <div className="sub-section-four">
                <h5>Reach out to us</h5>
                <p>
                  Kenton Sport Hall, Kenton School, Drayton Road, Kenton. NE3
                  3RU, UK{" "}
                </p>
                <p>07421706769</p>
                <p>info@hispresencenewcastle.org.uk</p>
                <div className="sub-footer-logo d-flex">
                  <Image src={Youtube} alt="youbube icon" />
                  <Image src={Instagram} alt="instagram fill" />
                </div>
              </div>
            </div>
          </div>
          <div className="horizontal-line"></div>
          <div className="copyright d-flex justify-content-center">
            <p>
              Copyright &copy;2023 All rights reserved | RCCG His presence
              Newcastle
            </p>
          </div>
        </section>
      </div>
    </footer>
  );
};

export default Footer;
