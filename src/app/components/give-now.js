import Give from "../assests/images/give.png";
import Image from "next/image";
import HomepageBanner from "../assests/images/church-service.png";
import Buttons from "./button";

const GiveNow = () => {
  return (
    <section>
      <div className="d-flex give-container justify-content-between">
        <div className="sub-give-container">
          <h5>Give now!</h5>
          <div className="sub-give-container-info">
            <h6>Account Name</h6>
            <p>RCCG His presence newcastle</p>
          </div>
          <div className="sub-give-container-info">
            <h6>Account Number</h6>
            <p>23796027</p>
          </div>
          <div className="sub-give-container-info">
            <h6>Sort Code</h6>
            <p>20-59-43</p>
          </div>
          <div className="sub-give-container-info">
            <h6>Bank Name</h6>
            <p>Barclays Bank</p>
          </div>
        </div>
        <div className="d-flex flex-column gap-3">
          <div className="give-img">
            <Image src={Give} alt="" c />
          </div>
          <div className="flex-fill">
            <div className="giving-reason">
              <p>
                “...give what you have decided in your heart to give, not
                reluctantly or under compulsion, for God loves a cheerful
                giver."
              </p>
            </div>
          </div>
        </div>
      </div>
      <div className="custom-container mt-4 mb-4">
        <div className="d-flex flex-column align-items-center justify-content-center">
          <div className="family-container text-center mb-5 px-4">
            <h3 className="fw-bold mb-3">Fit into our family</h3>
            <p className="text-center mb-4 mt-3">
              Are you in search of a family? At His presence, everyone's part of
              God's family. Join us for warmth, spiritual growth, lasting
              friendships, and profound grace
            </p>
            <Buttons
              text="Find out more"
              borderRadius="5px"
              customName="family-btn"
            />
          </div>
          <div>
            <Image src={HomepageBanner} alt="" />
          </div>
        </div>
      </div>
    </section>
  );
};

export default GiveNow;
