import Image from "next/image";


const WeeklyServices = ({
  title,
  subtitle,
  weeklyServices = [],
  sideImage,
  weeklyCustomClassses,
  bgColor = "#FFF",
  titleColor = "#1B1B1B",
}) => {
  return (
    <section
      style={{ backgroundColor: bgColor }}
      className={`${weeklyCustomClassses ? weeklyCustomClassses : ""}`}
    >
      <div className=" weekly-container">
        <div className="text-center text-white weekly-title-container">
          <h2>{title}</h2>
          <p>{subtitle}</p>
        </div>
        <div className="d-flex justify-content-between weekly-services-wrapper flex-column flex-lg-row">
          <div className="weekly-service-container">
            {weeklyServices.map(
              (
                { service, serviceDay, serviceIcon, serviceInfo, serviceTime },
                index
              ) => (
                <div key={`service-${index}`}>
                  <div className="d-flex sub-weekly-service-container">
                    <div>
                      <Image src={serviceIcon} alt="" className="service-icon" />
                    </div>
                    <div className="ms-3 mb-2">
                      <h5 style={{ color: titleColor }}>{service}</h5>
                      <p className="service-info-text">{serviceInfo}</p>
                      <div
                        className={`${weeklyCustomClassses} service-information`}
                      >
                        <p>{serviceDay}</p>
                        <p>{serviceTime}</p>
                      </div>
                    </div>
                  </div>
                </div>
              )
            )}
          </div>
          <div className="side-image-wrapper">
            <Image src={sideImage} alt="weekly-image" className="side-image" />
          </div>
        </div>
      </div>
    </section>
  );
};

export default WeeklyServices;
