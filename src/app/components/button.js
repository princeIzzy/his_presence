import Image from "next/image";

function Buttons({
  text,
  border = " 1px solid transparent",
  bgColor = "#DA241C",
  icon,
  textColor = "#ffffff",
  borderRadius,
  customName,
}) {
  return (
    <button
      className={`d-flex align-items-center justify-content-center ${customName}`}
      style={{
        backgroundColor: bgColor,
        border,
        color: textColor,
        borderRadius: borderRadius,
      }}
      type="button"
    >
      {icon ? <Image src={icon} alt="button icon" className="btn-icon" /> : null}{" "}
      {text}
    </button>
  );
}

export default Buttons;
