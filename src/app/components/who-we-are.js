import AboutUs from "../assests/images/about-us.png";
import Image from "next/image";
import Guest from "../assests/images/Guest.png"
import TextWithMedia from "./text-with-media";
import TogetherImage from "../assests/images/together.png";
import TextWithImage from "./TextWithImage";


const WhoWeAre = () => {
  return (
    <section>
      <div className="about-us d-flex flex-column align-content-center justify-content-center">
        <div className="text-center about-us-text">
          <p>ABOUT US</p>
          <h1 className="about-us-header">
          We are a community of people who love to follow Jesus
          </h1>
          <div className="about-us-image-container">
            <Image src={Guest} alt="About us" />
          </div>
          {/* <TextWithMedia
        media={Group37}
        title="Together we grow!"
        paragraph="We're dedicated to addressing spiritual, social, physical, mental, 
        and emotional needs, relying on God's strength through prayer and the Holy Spirit in our discipleship journey."
        bgcolor="#FFF"
      /> */}
            <TextWithImage
        media={TogetherImage}
        title="Together we grow!"
        paragraph="We're dedicated to addressing spiritual, social, physical, mental, and emotional needs, relying on God's strength through prayer and the Holy Spirit in our discipleship journey."
        bgcolor="#FFF"
        textColor="#FFA500"
        textBgcolor="rgba(255, 165, 0, 0.1)"
        customClasses="align-items-stretch"
      />
          <div className="sub-about-us">
            <h4 className="mt-lg-4">Who we are </h4>
            <div className="d-flex flex-column gap-4">
              <p>HPN (RCCG HIS PRESENCE NEWCASTLE) is a vibrant and inclusive community church that values diversity and unity. Individuals from various backgrounds, cultures, and walks of life gather together to worship and grow in faith as one body. </p>

              <p>We are committed to sharing our experience born out of encounters in God presence thereby creating an atmosphere and environment where everyone would have a genuine encounter with God and experience the fullness of God’s presence in  their lives.</p>

              <p>As ambassadors of God&#39;s love, we sow seeds of grace and kindness, fostering a global community united by the shared experience of God&#39;s unconditional love bringing men unto salvation.</p>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default WhoWeAre;
