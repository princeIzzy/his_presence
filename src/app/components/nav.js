"use client";

// import "bootstrap/dist/css/bootstrap.min.css";
import  Link  from "next/link";
import Logo from "../assests/images/logo.png";
import Listenlogo from "../assests/images/listen-logo.png";
import { useState, useRef, useEffect } from "react";
import Image from "next/image";
import { usePathname } from "next/navigation";

const Navigations = () => {
  const pathname =  usePathname();

  const [isNavCollapsed, setIsNavCollapsed] = useState(false);
  const handleNavCollapse = () => setIsNavCollapsed(!isNavCollapsed);
  const handleDropdown = (e) => { 
    const expanded = e.target.getAttribute("aria-expanded");
    e.target.setAttribute(
      "aria-expanded",
      expanded === "false" ? "true" : "false"
  );
  }
  const ref = useRef(null);

  useEffect(() => {
    if(pathname) {
      ref.current.setAttribute("aria-expanded", "false");
    }
    const handleOutSideClick = (event) => {
      if (!ref.current?.contains(event.target)) {
        ref.current.setAttribute("aria-expanded", "false");
      }
    };

    window.addEventListener("mousedown", handleOutSideClick);

    return () => {
      window.removeEventListener("mousedown", handleOutSideClick);
    };
  }, [ref, pathname]);

  return (
    <>
      <nav
        className={`navbar navbar-expand-lg ${
          pathname === "/"
            ? "position-absolute color-transparent w-100"
            : "position-relative color-white "
        }`}
      >
        <div className="container-fluid">
          <Link href="/" onClick={handleNavCollapse}>
            <Image  src={Logo} alt="" className="main-logo" />
          </Link>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarScroll"
            aria-controls="navbarScroll"
            aria-expanded="false"
            aria-label="Toggle navigation"
            onClick={handleNavCollapse}
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div
            className={`collapse navbar-collapse ${
              isNavCollapsed ? "show" : ""
            }`}
            id="navbarScroll"
          >
            <ul className="navbar-nav ms-auto my-2 my-lg-0 navbar-nav-scroll navigation-links d-flex justify-content-between align-items-center overflow-hidden">
              <span
                className={`
                  ${pathname === "/"
                    ? "navigation-link text-white"
                    : "navigation-link text-dark"}
                position-relative nav-dropdown-btn pointer-cursor d-flex align-items-center justify-content-center`}
                onClick={handleDropdown}
                data-dropdown="dropdown"
                aria-expanded="false"
                ref={ref}
              >
                  About us 
                  <svg className="ms-1" xmlns="http://www.w3.org/2000/svg" width="18" height="19" viewBox="0 0 18 19" fill="none">
                    <path d="M14.94 7.2124L10.05 12.1024C9.4725 12.6799 8.5275 12.6799 7.95 12.1024L3.06 7.2124" stroke="#1B1B1B" strokeWidth="1.5" strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round"/>
                  </svg>
                <ul id="dropdown" className="d-flex flex-column align-items-start justify-content-start position-absolute list-unstyled dropdown-container px-4">
                <li className="w-100">
                    <Link
                        className="dropdown-navigation-link text-dark"
                        onClick={handleNavCollapse}
                        href="/about-us"
                      >
                      About His Presence Newcastle Church
                    </Link>
                  </li>
                <li className="w-100">
                    <Link
                        className="dropdown-navigation-link text-dark"
                        onClick={handleNavCollapse}
                        href="/our-beliefs"
                      >
                      Our beliefs
                    </Link>
                  </li>
                  <li className="w-100">
                    <Link
                      className="dropdown-navigation-link text-dark"
                      onClick={handleNavCollapse}
                      href="/our-mission"
                    >
                      Our Missions
                    </Link>
                  </li>
                  <li className="w-100">
                    <Link
                        className="dropdown-navigation-link text-dark"
                        onClick={handleNavCollapse}
                        href="/our-leaders"
                      >
                      Leadership
                    </Link>
                  </li>
                </ul>
              </span>
              <Link
                className={
                  pathname === "/"
                    ? "navigation-link text-white"
                    : "navigation-link text-dark"
                }
                onClick={handleNavCollapse}
                href="/ministries"
              >
                Ministries
              </Link>
              <Link
                className={
                  pathname === "/"
                    ? "navigation-link text-white"
                    : "navigation-link text-dark"
                }
                onClick={handleNavCollapse}
                href="/givings"
              >
                Givings
              </Link>
              <Link
                className={
                  pathname === "/"
                    ? "navigation-link text-white"
                    : "navigation-link text-dark"
                }
                onClick={handleNavCollapse}
                href="/contact-us"
              >
                Contact Us
              </Link>
              <Link
                className={
                  pathname === "/"
                    ? "navigation-link text-white"
                    : "navigation-link text-dark"
                }
                onClick={handleNavCollapse}
                href="/live-stream"
              >
                <button className="btn color-button d-flex">
                  <Image  className="listen-logo" src={Listenlogo} alt="" />
                  Live Stream
                </button>
              </Link>
            </ul>
          </div>
        </div>
      </nav>
    </>
  );
}

export default Navigations;

