import Image from "next/image"

const ContactDetails = ({title, contactinfo=[]}) => {
    return(
        <section>
            <div className="contact-container d-flex align-items-center justify-content-center">
                <div>
                    <h4>{title}</h4>
                    <div className="d-flex sub-contact-container row flex-wrap row-gap-3">
                        {contactinfo.map(({contactTitle, contactIcon, contactInfo, contactBg = "#FEF6F6"}, index) =>(
                            <div key={`contact-${index}`} className="col-12 col-sm-6 col-md-3">
                                <div className="sub-contact-container-details">
                                    <div className=""style={{ backgroundColor: contactBg }}>
                                        <Image src={contactIcon} alt="" className="contact-img"/>
                                        <p className="contact-title">{contactTitle}</p>
                                        <p className="contact-info">{contactInfo}</p>
                                    </div>
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            </div>
        </section>
)
}

export default ContactDetails