import PersonContact from "../assests/images/person-contact.png";
import MailContact from "../assests/images/mail-contact.png";
import PhonebookContact from "../assests/images/phonebook-contact.png";
import Image from "next/image";

const ContactForm = () => {
  return (
    <section className="main-form-container ">
      <div className="form-container flex-column flex-lg-row custom-container">
        {/* <div className="form-container-img">
          <Image src={FormImage} alt="" />
        </div> */}
        <div className="form-details-container">
          <form action="/action_page.php">
            <h2>Send Us a Message</h2>
            <div className="input-container d-flex justify-content-between">
              <div className="form-details d-flex">
                <Image src={PersonContact} alt="" />
                <input
                  className="input-field"
                  type="text"
                  placeholder="First name*"
                  name="usrnm"
                />
              </div>
              <div className="form-details">
                <Image src={PersonContact} alt="" />
                <input
                  className="input-field"
                  type="text"
                  placeholder="Last name*"
                  name="usrnm"
                />
              </div>
            </div>
            <div className="input-container d-flex justify-content-between">
              <div className="form-details">
                <Image src={MailContact} alt="" />
                <input
                  className="input-field"
                  type="text"
                  placeholder="Your mail*"
                  name="usrnm"
                />
              </div>
              <div className="form-details">
                <Image src={PhonebookContact} alt="" />
                <input
                  className="input-field"
                  type="text"
                  placeholder="Your phone number"
                  name="usrnm"
                />
              </div>
            </div>
            <div className="input-container">
              <div className="form-details-message">
                <input
                  className="input-field"
                  type="text"
                  placeholder="Message"
                  name="usrnm"
                />
              </div>
            </div>

            <button type="submit" className="btn">
              Send
            </button>
          </form>
        </div>
      </div>
    </section>
  );
};

export default ContactForm;
