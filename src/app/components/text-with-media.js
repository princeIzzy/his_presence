import VideoCard from "./video-card";
import Ellipse5 from "../assests/images/Ellipse 5.png";
import Ellipse6 from "../assests/images/Ellipse 6.png";
import Ellipse7 from "../assests/images/Ellipse 7.png";
import Ellipse8 from "../assests/images/Ellipse 8.png";
import Ellipse10 from "../assests/images/Ellipse 10.png";
import Ellipse11 from "../assests/images/Ellipse 11.png";
import Ellipse12 from "../assests/images/Ellipse 12.png";
import Image from "next/image";

const TextWithMedia = ({
  media,
  title,
  paragraph,
  mediaType = "image",
  bgcolor = "red",
  mediaCardTitle = "food",
  mediaCardSubtitle,
  buttonText,
  buttonLink,
  reverse,
  linkIcon,
  customClasses,
  bgDots = false,
  textColor,
  textBgcolor
}) => {
  return (
    <section
      className={`${
        customClasses ? customClasses : ""
      } position-relative overflow-hidden py-5`}
      style={{ backgroundColor: bgcolor }}
    >
      <div
        className={`d-flex  justify-content-center align-items-center flex-column ${
          reverse
            ? "flex-sm-column-reverse flex-lg-row-reverse"
            : "flex-sm-column flex-lg-row"
        } ${customClasses ? customClasses : ""}`}
      >
        <div className="w-50 media-container">
          {mediaType === "image" ? (
            <Image src={media} className="mediaImage rounded-4" />
          ) : (
            <VideoCard
              title={mediaCardTitle}
              subtitle={mediaCardSubtitle}
              src={media}
            />
          )}
        </div>
        <div className={`w-50 text-container d-inline-flex flex-column align-items-center align-items-lg-start specific-padding ${textColor} ${textBgcolor}`} style={{ color: textColor, backgroundColor: textBgcolor}}>
          <h1>{title}</h1>
          <p style={{ color: textColor}}>{paragraph}</p>
          {buttonText && (
            <a
              href={buttonLink}
              className="linkbutton d-flex justify-content-center align-content-center relative"
            >
              {linkIcon && <Image src={linkIcon} alt="" className="w-2"/>} {buttonText}
            </a>
          )}
        </div>
        {bgDots && (
          <div className="bg-dots position-absolute w-100 h-100">
            <span className="bg-dot position-absolute">
              <Image src={Ellipse5} alt="" />
            </span>
            <span className="bg-dot position-absolute">
              <Image src={Ellipse6} alt="" />
            </span>
            <span className="bg-dot position-absolute">
              <Image src={Ellipse7} alt="" />
            </span>
            <span className="bg-dot position-absolute">
              <Image src={Ellipse8} alt="" />
            </span>
            <span className="bg-dot position-absolute">
              <Image src={Ellipse10} alt="" />
            </span>
            <span className="bg-dot position-absolute">
              <Image src={Ellipse11} alt="" />
            </span>
            <span className="bg-dot position-absolute">
              <Image src={Ellipse12} alt="" />
            </span>
          </div>
        )}
      </div>
    </section>
  );
};

export default TextWithMedia;
