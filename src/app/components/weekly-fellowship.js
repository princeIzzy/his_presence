import Button from "./button";
import Image from "next/image";

const WeeklyFellowship = ({ title, subtitle, bgImage, buttons = [] }) => {
  return (
    <div>
      <div className="position-relative weekly-container">
        <Image src={bgImage} alt="Banner" />
        <div className="position-absolute banner-overlaying">
          <div className="overlay weekly-align d-flex align-items-center justify-content-center flex-column">
            <h1 className="text-white">{title}</h1>
            <p className="text-white text-center">{subtitle}</p>

            <div className="d-flex">
              {buttons.map(
                ({ text, bgColor, border, textColor, borderRadius }, index) => (
                  <Button
                    key={`buttn-${index}`}
                    text={text}
                    bgColor={bgColor}
                    border={border}
                    textColor={textColor}
                    borderRadius={borderRadius}
                  />
                )
              )}
            </div>
          </div>
        </div>
        <div className="position-absolute banner-overlay" />
      </div>
    </div>
  );
};

export default WeeklyFellowship;
