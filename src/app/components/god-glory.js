import Button from "./button";
import HappyMan from "../assests/images/happy-man.png";
import HappyLady from "../assests/images/happy-lady.png";
import Image from "next/image";

const GodGlory = ({ buttons = [] }) => {
  return (
    <section className="bg-grey">
      <div className="god-glory-container">
        <div className="w-50 sub-god-glory-container">
          <div className="god-glory-text-wrapper d-flex flex-column text-center text-lg-start align-items-center align-items-lg-start">
            <h4>We live for God’s glory</h4>
            <p>
              RCCG HIS PRESENCE exists for the glory of God, that, through the
              preaching of the Gospel, men and women will experience the saving
              and transforming power of Jesus Christ.
            </p>

            <div className="d-flex">
              {buttons.map(
                ({ text, bgColor, borderColor, textColor }, index) => (
                  <Button
                    key={`buttn-${index}`}
                    text={text}
                    bgColor={bgColor}
                    borderColor={borderColor}
                    textColor={textColor}
                  />
                )
              )}
            </div>
          </div>
        </div>
        <div className="god-glory-img-container d-flex gap-3 w-50 p-3 p-lg-0">
          <Image src={HappyLady} alt="happylady" className="position-relative first-god-glory-img-container" />
          <Image src={HappyMan} alt="happyman" className="position-relative second-god-glory-img-container"/>
        </div>
      </div>
    </section>
  );
};

export default GodGlory;
