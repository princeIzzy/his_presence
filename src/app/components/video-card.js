const VideoCard = ({ src, title, subtitle }) => {
  return (
    <div className="card">
      <video className="card-video" src={src} controls autoPlay={false}></video>
      <div className="card-body">
        <h5 className="card-title">{title}</h5>
        <p className="card-text">{subtitle}</p>
      </div>
    </div>
  );
};

export default VideoCard;
