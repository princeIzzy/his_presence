import TextWithMedia from "../components/text-with-media";
import FamilyLove from "../assests/images/about-us-two.png";
import WhoWeAre from "../components/who-we-are";
import ImageTwo from "../assests/images/image-two.png";
import Headphone from "../assests/images/Headphone.png";
import GodGlory from "../components/god-glory";
import TextWithImage from "../components/TextWithImage";

export default function AboutUs() {
  const weeklyBtn = [
    {
      text: "Find out more",
      borderColor: "transparent",
      bgColor: "#DA241C",
    },
  ];

  return (
    <>
      <WhoWeAre />
      <GodGlory buttons={weeklyBtn} />

      <TextWithImage
        media={FamilyLove}
        title="A family of love"
        paragraph="His Presence is a place where love thrives, compassion blossoms, 
        and forgiveness overflows. Join us in fellowship and experience the warmth of genuine love as we journey together.
        "
        bgcolor="#FFF"
        reverse={true}
        textColor="#DA241C"
        textBgcolor="rgb(218,36,28,0.2)"
        customClasses="align-items-stretch"
      />

      <TextWithMedia
        media={ImageTwo}
        title="Stay connected always"
        paragraph="We invite you to join us live and be part of our church meetings
          to experience the uplifting and inspiring messages firsthand."
        bgcolor="rgba(244, 244, 244, 0.90)"
        buttonLink="/live-stream"
        buttonText="Listen Live"
        linkIcon={Headphone}
        customClasses="stay-connected"
        bgDots={true}
      />
    </>
  );
}
