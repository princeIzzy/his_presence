import Image from "next/image";
import TextWithMedia from "../components/text-with-media";
import Headphone from "../assests/images/Headphone.png";
import ImageTwo from "../assests/images/image-two.png";
import Pastors from "../assests/images/Pastor Emmanuel & Pastor Mrs Omotayo Alawode 1.png";

const OurBeliefs = () => {
  return (
    <div>
      <div>
        <div className="container">
          <div className="row flex-column flex-lg-row">
            <div className="col">
              <Image
                src={Pastors}
                alt=""
                className="rounded-4 h-100 object-fit-cover"
              />
            </div>
            <div className="col">
              <div className="pastors-container">
                <h2 className="fw-bold">Who the Lord has set over us</h2>
                <p>
                  On the 17 th of March 2019, RCCG HIS PRESENCE NEWCASTLE parish
                  was inaugurated as a parish of The Redeemed Christian Church
                  of God – a worldwide mission established in 1952.
                </p>
                <p>
                  Pastor Emmanuel &amp; Omotayo Alawode were both sent forth
                  from Manchester (RCCG HIS PRESENCE ASSEMBLY, SALFORD under the
                  leadership of Pastor Tunde Alli) to continue the work of God
                  here in Newcastle as Parish Pastors after serving in various
                  capacities as Ministers for a period of about seven years in
                  Manchester.
                </p>
                <p>
                  To the glory of God, the church is marching on from when the
                  Parish started in 2019 with just Pastor Emmanuel, Pastor Mrs
                  Omotayo and their two little children. They are both
                  passionate about Soul winning and kingdom advancement.
                </p>
                <p>
                  Pastor Emmanuel’s ministerial journey started right from a
                  young age having being involved in Campus Evangelism.
                </p>
                <p>
                  He later went to the University and served at various
                  capacities leading the Campus Bible Study Team (Redeemed
                  Christian Fellowship) RCF and as an Exco from 2005 to 2007
                </p>
              </div>
            </div>
          </div>

          <TextWithMedia
            media={ImageTwo}
            title="Stay connected always"
            paragraph="We invite you to join us live and be part of our church meetings
                    to experience the uplifting and inspiring messages firsthand."
            bgcolor="#ffffff"
            buttonLink="https://www.google.com/"
            buttonText="Listen Live"
            linkIcon={Headphone}
            customClasses="stay-connected"
            bgDots={true}
          />
        </div>
      </div>
    </div>
  );
};

export default OurBeliefs;
