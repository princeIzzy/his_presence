import BibleStudy from "../assests/images/bible-study.png";
import PrayerMeeting from "../assests/images/prayer-meeting.png";
import ChurchService from "../assests/images/sunday-service.png";
import MinistriesImage from "../assests/images/Frame 280.png";
import WeeklyServices from "../components/weekly-services";
import Expectation from "../components/expectation";
import ExpectationImage from "../assests/images/rectangle.png";
import TextWithMedia from "../components/text-with-media";
import ImageTwo from "../assests/images/image-two.png";
import Headphone from "../assests/images/Headphone.png";
import Bible from "../assests/images/bible.png";
import SundayService from "../assests/images/sundayService.png";
import Image from "next/image";

const Ministries = () => {
  return (
    <div>
      <div>
        <div>
          <h2 className="text-center">Our weekly meetings</h2>
          <div className="container pt-5">
            <div className="row gap-3">
              <div className="col ministry-border text-center">
                <Image src={SundayService} alt="" className="ministry-img" />
                <h5 className="pt-5">Sunday service</h5>
                <p>
                  Sunday Service starts at 10:00am with soul lifting praise and
                  worship songs.{" "}
                </p>
              </div>
              <div className="col ministry-border text-center">
                <Image src={Bible} alt="" className="ministry-img" />
                <h5 className="pt-5">Bible Study</h5>
                <p>
                  Every Wednesday from 6:30pm-7:30pm An hour of digging deep
                  into the word of God, question and answer time{" "}
                </p>
              </div>
              <div className="col ministry-border text-center">
                <Image src={PrayerMeeting} alt="" className="ministry-img" />
                <h5 className="pt-5">Prayer Meeting</h5>
                <p>
                  Sunday Service starts at 10:00am with soul lifting praise and
                  worship songs.{" "}
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <Expectation
        bgImage={ExpectationImage}
        mainTitle="What to expect from His presence?"
      />
      <TextWithMedia
        media={ImageTwo}
        title="Stay connected always"
        paragraph="We invite you to join us live and be part of our church meetings
          to experience the uplifting and inspiring messages firsthand."
        bgcolor="#fff"
        buttonLink="https://www.google.com/"
        buttonText="Listen Live"
        linkIcon={Headphone}
        customClasses="stay-connected"
        bgDots={true}
      />
    </div>
  );
};

export default Ministries;
