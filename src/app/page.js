import Banner from "./components/banner";
import Listenlogo from "./assests/images/listen-logo.png";
import HomepageBanner from "./assests/images/church-service.png";
import TextWithMedia from "./components/text-with-media";
import WeeklyFellowship from "./components/weekly-fellowship";
import WeeklyImg from "./assests/images/weekly-img.png";
import ImageTwo from "./assests/images/image-two.png";
// import Headphone from "./assests/images/homepage-banner.jpg";
import Headphone from "./assests/images/Headphone.png";
import PstEmma from "./assests/images/pastor-emma.png";
import Ministration from "./assests/images/Ministration.png";
import ChoirMinistration from "./assests/images/Choir-Ministration.png"
import Buttons from "./components/button";
import Image from "next/image";

export default function LandingPage() {
  
  const buttons = [
    {
      text: "Live stream",
      border: "transparent",
      bgColor: "#DA241C",
      icon: Listenlogo,
      borderRadius: "5px",
    },
    {
      text: "Give now",
      border: " 1px solid #F9FAF9",
      bgColor: "transparent",
      borderRadius: "5px",
      customName: "transparent-hover"
    },
  ];
  const weeklyBtn = [
    {
      text: "Find out more",
      border: "1px solid #F9FAF9",
      bgColor: "transparent",
      borderRadius: "5px",
    },
  ];

  return (
    <>
      <Banner
        title="Bringing together Faith Welcome to his presence"
        subtitle="Welcome to our faith-filled haven, dedicated to nurturing souls for Christ "
        buttons={buttons}
        bgImage={HomepageBanner}
        customSubtitleClass="landingpage-subtitle"
        bannerheight="big"
      />
      {/* <Banner b bgImage={HomepageBanner} /> */}
      <div className="bg-white service-timer">
        <div className="custom-container d-flex justify-content-between  align-items-center">
          <h1 className="service-timer-title">Upcoming Church Service</h1>
          <div className="d-flex gap-large flex-column flex-md-row">
            <div className="d-flex  align-items-center">
              <p className="service-timer-num">05</p>
              <h4 className="service-timer-time">Days</h4>
            </div>
            <div className="d-flex align-items-center">
              <p className="service-timer-num">21</p>
              <h4 className="service-timer-time">Hours</h4>
            </div>
            <div className="d-flex align-items-center">
              <p className="service-timer-num">12</p>
              <h4 className="service-timer-time">Mintue</h4>
            </div>
            <div className="d-flex align-items-center">
              <p className="service-timer-num">05</p>
              <h4 className="service-timer-time">Seconds</h4>
            </div>
          </div>
        </div>
      </div>
      <TextWithMedia
        title="We are guided by faith and united in love"
        paragraph="We believe that God loves to do extraordinary things through ordinary people. We believe that this means we can be honest and sincere with one another. We believe that this creates an atmosphere of authenticity in which people can flourish and Christian faith can grow"
        media={PstEmma}
        bgcolor="#F4F4F4"

      />
     
      <div className="new-section">
        <div className="custom-container">
          <div className="d-flex align-items-center flex-column">
            <h2 className="fw-bold">Come let’s journey together</h2>
            <p className="new-section-p">We're a community committed to daily following Jesus,
              believing in God's boundless love and the power of living
              like Jesus to transform cities from within.
            </p>
            <Buttons text="Find out more" borderRadius="5px"/>
          </div>
          <div className="pt-5">
            <Image src={Ministration} alt="" className="rounded-4"/>
          </div>
        </div>
      </div>
      
      <WeeklyFellowship
        title="Fellowship with us every week"
        subtitle="We offer diverse spiritual opportunities, such as Bible studies, 
        virtual prayer meetings, and our Sunday service, to support your faith journey and growth."
        buttons={weeklyBtn}
        bgImage={ChoirMinistration}
      />
      <TextWithMedia
        media={ImageTwo}
        title="Stay connected always"
        paragraph="We invite you to join us live and be part of our church meetings
         to experience the uplifting and inspiring messages firsthand."
        bgcolor="#ffffff"
        buttonLink="https://www.google.com/"
        buttonText="Listen Live"
        linkIcon={Headphone}
        customClasses="stay-connected"
        bgDots={true}
      />
    </>
  );
}
