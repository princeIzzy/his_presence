import Banner from "../components/banner";
import ContactUsImage from "../assests/images/contact-us.png";
import GiveNow from "../components/give-now";
import TextWithMedia from "../components/text-with-media";
import ImageTwo from "../assests/images/image-two.png";
import Headphone from "../assests/images/Headphone.png";

export default function Givings() {
  return (
    <>
      <Banner
        bgImage={ContactUsImage}
        title="Give to support our ministry"
        subtitle="Every contribution, no matter the size, helps in advancing our faith community "
        customTitleClass="giving-title"
        customSubtitleClass="giving-subtitle"
        customClasses="giving-banner"
      />
      <GiveNow />
      <TextWithMedia
        media={ImageTwo}
        title="Stay connected always"
        paragraph="We invite you to join us live and be part of our church meetings
         to experience the uplifting and inspiring messages firsthand."
        bgcolor="#ffffff"
        buttonLink="https://www.google.com/"
        buttonText="Listen Live"
        linkIcon={Headphone}
        customClasses="stay-connected"
        bgDots={true}
      />
    </>
  );
}
