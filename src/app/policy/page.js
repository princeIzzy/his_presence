const Policy = () => {
  return (
    <section className="custom-container">
      <div className="policy-container mt-5">
        <div className="sub-policy-container mb-5">
          <h2 className="fw-bolder">Privacy Policy</h2>
          <p>Last visited April 10, 2024</p>
        </div>
        <div>
          <div className="pb-4">
            <h5 className="fw-bold pb-1">About</h5>
            <p>
              We are committed to safeguarding and preserving the privacy of our
              visitors. This Privacy Policy explains what happens to any
              personal data that you provide to us, or that we collect from you
              whilst you visit our site. We do update this Policy from time to
              time so please do review this Policy regularly.
            </p>
          </div>
          <div className="pb-4">
            <h5 className="fw-bold pb-1">Information we collect</h5>
            <p>
              In running and maintaining our website we may collect and process
              the following data about you:
            </p>

            <p>
              Information about your use of our site including details of your
              visits such as pages viewed and the resources that you access.
              Such information includes traffic data, location data and other
              communication data. Information provided voluntarily by you. For
              example, when you register for information or make a purchase.
            </p>

            <p>
              Information that you provide when you communicate with us by any
              means.
            </p>
          </div>
          <div className="pb-4">
            <h5 className="fw-bold pb-1">Use of Your Information</h5>
            <p>
              We use the information that we collect from you to provide our
              services to you. In addition to this we may use the information
              for one or more of the following purposes:
            </p>

            <p>
              To provide information to you that you request from us relating to
              our products or services.
            </p>

            <p>
              To inform you of any changes to our website, services and
              products.
            </p>

            <p>
              To provide information to you relating to other services that may
              be of interest to you. Such additional information will only be
              provided where you have consented to receive such information.
            </p>

            <p>
              If you have previously purchased services from us we may provide
              to you details of similar services, or services, that you may be
              interested in. Where your consent has been provided in advance we
              may allow selected third parties to use your data to enable them
              to provide you with information regarding unrelated services which
              webelieve may interest you. Where such consent has been provided
              it can be withdrawn by you at any time.
            </p>
          </div>
          <div className="pb-4">
            <h5 className="fw-bold pb-1">Storing Yours Personal Data</h5>
            <p>
              In operating our website it may become necessary to transfer data
              that we collect from you to locations outside of the European
              Union for processing and storing. By providing your personal data
              to us, you agree to this transfer,storing or processing.
            </p>

            <p>
              We do our upmost to ensure that all reasonable steps are taken to
              make sure that your data is treated stored securely. Unfortunately
              the sending of information via the internet is not totally secure
              and on occasion such information can be intercepted. We cannot
              guarantee the security of data that you choose to send us
              electronically, sending such information is entirely at your own
              risk.
            </p>
          </div>
          <div className="pb-4">
            <h5 className="fw-bold pb-1">Third Party Links</h5>
            <p>
              On occasion we include links to third parties on this website.
              Where we provide a link it does not mean that we endorse or
              approve that site’s policy towards visitor privacy. You should
              review their privacy policy before sending them any personal data.
            </p>
          </div>
          <div className="pb-4">
            <h5 className="fw-bold pb-1">Use Of Cookies</h5>
            <p>
              Cookies provide information regarding the computer used by a
              visitor. We may use cookies where appropriate to gather
              information about your computer in order to assist us in improving
              our website. We may gather information about your general internet
              use by using cookies.
            </p>

            <p>
              Where used, these cookies are downloaded to your computer and
              stored on the computer’s hard drive. Such information will not
              identify you personally. It is statistical data. This statistical
              data does not identify any personal details whatsoever.
            </p>

            <p>
              You can adjust the settings on your computer to decline any
              cookies if you wish. This can easily be done by activating the
              reject cookies setting on your computer. Our advertisers may also
              use cookies, over which we have no control. Such cookies (if used)
              would be downloaded once you click on advertisements on our
              website.
            </p>
            <p>
              {" "}
              We will not disclose your personal information to any other party
              other than in accordance with this Privacy Policy and in the
              circumstances detailed below:
            </p>

            <p>
              Where we are legally required by law to disclose your personal
              information. To prevent fraud or reduce the risk of fraud.
            </p>
          </div>
          <div className="pb-4">
            <h5 className="fw-bold pb-1">Access to Information</h5>
            <p>
              In accordance with the Data Protection Act 2018 you have the right
              to access any information that we hold relating to you. Please
              note that we reserve the right to charge a fee to cover costs
              incurred by us in providing you with the information.
            </p>
          </div>
          <div className="pb-4">
            <h5 className="fw-bold pb-1">Get In Touch</h5>
            <p>
              Please do not hesitate to contact us regarding any matter relating
              to this via email, info@hispresencenewcastle.org.uk
            </p>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Policy;
