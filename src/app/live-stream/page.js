import Banner from "../components/banner";
import LiveStream from "../assests/images/live-stream.png";
import LiveStreamImage from "../assests/images/iPhone 13 Pro Max - 1.png";
import Buttons from "../components/button";
import PathBg from "../assests/images/path14696-1.png";
import PathBg1 from "../assests/images/path14696.png";
import Image from "next/image";

const Livestream = () => {
  return (
    <>
      <Banner
        bgImage={LiveStream}
        title="Distance is no hindrance, listen on the go!"
        customTitleClass="livestream-title"
        customClasses="banner-livestream"
      />
      <div>
        <div className="position-relative path-container d-none d-lg-block">
          <Image src={PathBg} alt="" className="position-absolute path-bg" />
          <Image src={PathBg1} alt="" className="position-absolute path-bg1" />
        </div>
        <div className="d-flex flex-column justify-content-center livestream-container">
          <Image src={LiveStreamImage} alt="" className="livestream-img" />
          <div className="d-flex justify-content-between live-stream-btn">
            <Buttons text="Watch Live" customName="livestream-hover" />
            <Buttons
              text="Listen via waystream"
              bgColor="transparent"
              border="1px solid #1B1B1B"
              textColor="#000"
              customName="livestream-hover2"
            />
          </div>
        </div>
      </div>
    </>
  );
};

export default Livestream;
