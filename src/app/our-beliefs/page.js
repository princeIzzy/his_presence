
import Image from "next/image";
import TextWithMedia from "../components/text-with-media";
import Headphone from "../assests/images/Headphone.png";
import ImageTwo from "../assests/images/image-two.png";
import Beliefs from "../assests/images/belief.png"

const OurBeliefs= () => {
   
    return (
      <div>
        <div>
            <div className="custom-container">
                <h1 className="text-center py-5">We hold firm to truth</h1>
                <div>
                    <Image src={Beliefs} alt=""/>
                </div>
            </div>
            <div className="belief-container">
                <div className="pt-5">
                    <h2 className="fs-1 fw-bold container">Our core beliefs</h2>
                    <div className="custom-container pb-5 row align-items-lg-start">
                        <div className="d-flex flex-column gap-3 pt-5 align-items-start col-12 col-md-6 col-lg-4">
                            <div className="col defined-border rounded-3 d-flex justify-content-center align-items-center flex-column">
                                <p className="fw-bolder defined-title">God the Father</p>
                                <p className="defined-p">
                                    Creator of Heaven and Earth, who made
                                    man in His own image and likeness. 
                                    1 Corinthians 8:6. </p>
                            </div>
                            <div className="col defined-border rounded-3 d-flex justify-content-center align-items-center flex-column">
                                <p className="fw-bolder defined-title">The  death & resurrection of Jesus
                                    Christ
                                </p>
                                <p className="defined-p"> Jesus was crucified and died for the sins
                                    of mankind on the cross at Calvary, was 
                                    buried and rose from the dead on the 
                                    third day. He ascended into Heaven and 
                                    is forever seated at the right hand of 
                                    God the Father. Luke 24:6-7, 1 Thes 4:14</p>
                            </div>
                            <div className="col defined-border rounded-3 d-flex justify-content-center align-items-center flex-column">
                                <p className="fw-bolder defined-title">Baptism of the Holy Ghost
                                </p>
                                <p className="defined-p"> The Comforter and Teacher of all 
                                    things who dwell within us, uniting 
                                    us to Jesus Christ. Acts 16:6-7</p>
                            </div>
                        </div>
                        <div className="d-flex flex-column gap-3 pt-5 align-items-start  col-12 col-md-6 col-lg-4">
                            <div className="col defined-border rounded-3 d-flex justify-content-center align-items-center flex-column">
                                <p className="fw-bolder defined-title">The Holy Trinity</p>
                                <p className="defined-p">The three persons of the Godhead; God 
                                    the Father, God the Son, and God the 
                                    Holy Spirit Three in One. John 14:16-17, 
                                    15:26 </p>
                            </div>
                            <div className="col defined-border rounded-3 d-flex justify-content-center align-items-center flex-column">
                                <p className="fw-bolder defined-title">The Virgin birth</p>
                                <p className="defined-p">Jesus was conceived by the Holy Spirit 
                                    and born of the Virgin Mary. Matt 1:18-25</p>
                            </div>
                            <div className="col defined-border rounded-3 d-flex justify-content-center align-items-center flex-column">
                                <p className="fw-bolder defined-title">The Second coming of Jesus Christ</p>
                                <p className="defined-p">Jesus will come again to Earth in glory 
                                    and the dead in Christ will rise and those
                                    who are alive in Christ will be translated 
                                    into the presence of God for eternity. 
                                    Acts 1:11, Philippians 3:20</p>
                            </div>

                        </div>
                        <div className="d-flex flex-column gap-3 pt-5 align-items-start  col-12 col-md-6 col-lg-4">
                            <div className="col defined-border rounded-3 d-flex justify-content-center align-items-center flex-column">
                                <p className="fw-bolder defined-title">Holy Spirit</p>
                                <p className="defined-p">Jesus was conceived by the Holy Spirit
                                    and born of the Virgin Mary. 
                                    Matt 1:18-25</p>
                            </div>
                            <div className="col defined-border rounded-3 d-flex justify-content-center align-items-center flex-column">
                                <p className="fw-bolder defined-title">Salvation</p>
                                <p className="defined-p">A gift from God through grace and faith
                                    in Jesus Christ; that all who believe in 
                                    Him may be saved by turning from sin to
                                    repentance, trusting in His death and 
                                    resurrection and are born again by the
                                    Holy Spirit. John 3:16 </p>
                            </div>
                            <div className="col defined-border rounded-3 d-flex justify-content-center align-items-center flex-column">
                                <p className="fw-bolder defined-title">The holy Bible</p>
                                <p className="defined-p">The infallible and authoritative word of
                                    God given to direct all men and women 
                                    to salvation. Hebrews 4:12, 2 
                                    Timothy 3:16-17 </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           
           <TextWithMedia
                    media={ImageTwo}
                    title="Stay connected always"
                    paragraph="We invite you to join us live and be part of our church meetings
                    to experience the uplifting and inspiring messages firsthand."
                    bgcolor="#ffffff"
                    buttonLink="https://www.google.com/"
                    buttonText="Listen Live"
                    linkIcon={Headphone}
                    customClasses="stay-connected"
                    bgDots={true}
                />
        </div>
      </div>
    );
  };
  
  export default OurBeliefs;